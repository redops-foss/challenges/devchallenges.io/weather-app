import { getEnvironmentVariable } from './env';

describe('config/env', () => {
  let env;

  beforeAll(() => {
    env = window.env;
    window.env = {
      ...env,
      TEST_CONFIG_ENV_VAR_EXISTS: 'Ryan Reynolds',
      TEST_CONFIG_ENV_VAR_NOT_SET: `#{${process.env.ENV_PREFIX}TEST_CONFIG_ENV_VAR_NOT_SET}#`,
    };

    delete window.env.TEST_CONFIG_ENV_VAR_NOT_EXISTS;
  });

  afterAll(() => {
    window.env = env;
  });

  describe('getEnvironmentVariable', () => {
    it('should retrieve an existing environment variable', () => {
      expect(
        getEnvironmentVariable('TEST_CONFIG_ENV_VAR_EXISTS'),
      )
        .toBe('Ryan Reynolds');
    });

    it('should return null if variable does not exists', () => {
      expect(
        getEnvironmentVariable('TEST_CONFIG_ENV_VAR_NOT_EXISTS'),
      )
        .toBe(null);
    });

    it('should return null if variable is not set', () => {
      expect(
        getEnvironmentVariable('TEST_CONFIG_ENV_VAR_NOT_SET'),
      )
        .toBe(null);
    });
  });
});
