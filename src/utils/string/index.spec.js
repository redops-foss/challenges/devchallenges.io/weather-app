import { removeTrailingSlash } from './index';

describe('utils/string', () => {
  describe('removeTrailingSlash', () => {
    it('should remove the trailing slash if it exists', () => {
      expect(removeTrailingSlash('justin/')).toBe('justin');
    });

    it('should not remove any character if there is no trailing slash', () => {
      expect(removeTrailingSlash('bieber')).toBe('bieber');
    });
  });
});
