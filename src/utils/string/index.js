export const removeTrailingSlash = str => str.replace(/\/$/, '');
