import React, { useEffect } from 'react';
import { cleanup } from '@testing-library/react';
import render from '__tests_utils__/render/redux';
import { initialState, mapStateToProps, reducer } from '__mockdata__/utils/redux/counter';

import connect from './connect';

describe('utils/redux/connect', () => {
  afterEach(cleanup);

  it('should not pass dispatch to props if no mapDispatchToProps is provided', () => {
    const Component = ({ dispatch }) => {
      useEffect(() => {
        expect(dispatch).toBeUndefined();
      }, []);
      return null;
    };

    expect.assertions(1);

    const ConnectedComponent = connect(mapStateToProps)(Component);

    render(<ConnectedComponent />, { initialState, reducer });
  });
});
