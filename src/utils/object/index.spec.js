import { getEmptyObject, emptyObject } from 'utils/object';

describe('utils/object', () => {
  describe('getEmptyObject', () => {
    it('should return the empty object', () => {
      expect(getEmptyObject()).toBe(emptyObject);
    });

    it('should not return another empty object', () => {
      expect(getEmptyObject() === {}).toBe(false);
    });

    it('should always return the same empty object', () => {
      expect(getEmptyObject()).toBe(getEmptyObject());
    });

    it('should return an empty object', () => {
      expect(Object.keys(getEmptyObject()).length).toBe(0);
    });
  });
});
