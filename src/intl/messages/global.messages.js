import { defineMessages } from 'react-intl';

export default defineMessages({
  OK: { defaultMessage: 'OK' },
  CANCEL: { defaultMessage: 'Cancel' },
  SUCCESS: { defaultMessage: 'Success' },
});
