import React from 'react';
import ReactDOM from 'react-dom';
import dayjs from 'dayjs';

import store from 'redux/store';
import { setLocale } from 'redux/intl/actions';
import bootstrapDayJs from 'services/dayjs/bootstrap';

import App from 'react/App';

import { startServer } from '__mirage__';

import './style/main.scss';

startServer({ environment: 'development', trackRequests: true });

(async function main() {
  await Promise.all([
    bootstrapDayJs(dayjs),
    store.dispatch(setLocale()),
  ]);

  ReactDOM.render(
    <App />,
    document.getElementById('app'),
  );
}());
