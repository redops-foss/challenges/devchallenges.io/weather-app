import React from 'react';
import { render } from '@testing-library/react';
import { node } from 'prop-types';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

export default (
  ui,
  {
    initialState = {},
    reducer = v => v,
    store = createStore(reducer, initialState),
    ...renderOptions
  } = {},
) => {
  const Wrapper = ({ children }) => (
    <Provider store={store}>
      {children}
    </Provider>
  );

  Wrapper.propTypes = {
    children: node.isRequired,
  };

  return render(ui, { wrapper: Wrapper, ...renderOptions });
};
