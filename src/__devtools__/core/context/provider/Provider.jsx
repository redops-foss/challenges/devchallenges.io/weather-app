import React, {
  memo,
  useState,
  useMemo,
  useCallback,
} from 'react';
import PropTypes from 'prop-types';
import { uniqueId, omit, without } from 'lodash';

import { DevToolsContext } from '../index';

const DevToolsProvider = ({ children }) => {
  const [actionsMap, onChangeActionsMap] = useState({});
  const [actionsOrder, onChangeActionsOrder] = useState([]);

  // Build actions from actions order and actions map.
  const actions = useMemo(
    () => actionsOrder.map(id => actionsMap[id]),
    [actionsMap, actionsOrder],
  );

  // Generate a new dev tool action id and add the action to the map of registered actions.
  const registerDevTool = useCallback(
    (manifest, props) => {
      const id = uniqueId('devTool');

      onChangeActionsMap(currentActionsMap => ({
        ...currentActionsMap,
        [id]: {
          id,
          manifest,
          props,
        },
      }));

      onChangeActionsOrder(currentActionsOrder => [...currentActionsOrder, id]);

      return id;
    },
    [onChangeActionsMap, onChangeActionsOrder],
  );

  // Update the current props of a dev tool action.
  const updateDevTool = useCallback(
    (devToolId, props) => {
      onChangeActionsMap(currentActionsMap => (
        currentActionsMap[devToolId]
          ? {
            ...currentActionsMap,
            [devToolId]: {
              ...currentActionsMap[devToolId],
              props,
            },
          }
          : currentActionsMap
      ));
    },
    [onChangeActionsMap],
  );

  // Remove a dev tool action.
  const unregisterDevTool = useCallback(
    (devToolId) => {
      onChangeActionsMap(currentActionsMap => omit(currentActionsMap, [devToolId]));
      onChangeActionsOrder(currentActionsOrder => without(currentActionsOrder, devToolId));
    },
    [onChangeActionsMap, onChangeActionsOrder],
  );

  // Build the context value.
  const contextValue = useMemo(
    () => ({
      actions,
      registerDevTool,
      unregisterDevTool,
      updateDevTool,
    }),
    [
      actions,
      registerDevTool,
      unregisterDevTool,
      updateDevTool,
    ],
  );

  return (
    <DevToolsContext.Provider value={contextValue}>
      {children}
    </DevToolsContext.Provider>
  );
};

DevToolsProvider.displayName = 'DevToolsProvider';

DevToolsProvider.propTypes = {
  /** Provider children. */
  children: PropTypes.node,
};

DevToolsProvider.defaultProps = {
  children: null,
};

export default memo(DevToolsProvider);
