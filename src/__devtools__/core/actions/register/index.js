import { pick } from 'lodash/fp';
import { compose, lifecycle } from 'recompose';
import shallowEqual from 'shallowequal';

import omitProps from 'react/generic/props/omitProps';
import { withDevToolsContext } from '../../context/withDevToolsContext';

const devToolContextProps = [
  'registerDevTool',
  'unregisterDevTool',
  'updateDevTool',
];

/**
 * Return a HOC that registers / unregisters / updates the dev tool action of the manifest.
 * In production, return an empty HOC.
 *
 * @param {object} manifest - Dev tool manifest.
 * @param {string[]} [propsToOmit] - Props to omit for the wrapped component.
 * @returns {Function} HOC.
 */
export const withRegisterDevTool = (manifest, propsToOmit = devToolContextProps) => compose(
  withDevToolsContext(pick(devToolContextProps)),

  lifecycle({
    /** Register the dev tool action. */
    componentDidMount() {
      this.devToolId = this.props.registerDevTool(
        manifest,
        manifest.mapProps(this.props),
      );
    },

    /**
     * Update the dev tool action.
     *
     * @param {object} prevProps - Previous props.
     */
    componentDidUpdate(prevProps) {
      const { mapProps } = manifest;
      const mappedProps = mapProps(this.props);

      if (!shallowEqual(mapProps(prevProps), mappedProps)) {
        this.props.updateDevTool(this.devToolId, mappedProps);
      }
    },

    /** Unregister the dev tool action. */
    componentWillUnmount() {
      this.props.unregisterDevTool(this.devToolId);
    },
  }),

  omitProps(propsToOmit),
);
