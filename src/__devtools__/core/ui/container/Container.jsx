import React, { memo, useContext, useMemo } from 'react';

import { DevToolsContext } from '../../context';
import { groupByGroup } from '../../manifest';

import DevToolGroup from '../group/Group';

import classNames from './Container.module.scss';

const DevToolsUiContainer = () => {
  const { actions } = useContext(DevToolsContext);

  const actionsByGroup = useMemo(
    () => groupByGroup(actions),
    [actions],
  );

  return (
    <div className={classNames.container}>
      <h1 className={classNames.title}>Dev tools</h1>

      <div className={classNames.actionsContainer}>
        { Object.keys(actionsByGroup).map(group => (
          <DevToolGroup
            key={group}
            group={group}
            actions={actionsByGroup[group]}
          />
        )) }
      </div>
    </div>
  );
};

DevToolsUiContainer.displayName = 'DevToolsUiContainer';

export default memo(DevToolsUiContainer);
