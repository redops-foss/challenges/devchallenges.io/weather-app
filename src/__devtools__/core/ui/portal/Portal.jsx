import React from 'react';
import { createPortal } from 'react-dom';

import DevToolsUiContainer from '../container/Container';

import classNames from './Portal.module.scss';

export default class DevToolsUiPortal extends React.PureComponent {
  domNode = null;

  state = {
    isVisible: false,
  };

  /**
   * Create a DOM node to display the dev tools.
   *
   * @param {...any} args - Arguments to pass to Component constructor.
   */
  constructor(...args) {
    super(...args);

    window.addEventListener('keyup', this.onToggleVisible);
  }

  /** Cleanup. */
  componentWillUnmount() {
    this.removeDomNode();
    window.removeEventListener('keyup', this.onToggleVisible);
  }

  onToggleVisible = (event) => {
    // ctrl + ²
    if (event.ctrlKey && event.keyCode === 222) {
      if (this.state.isVisible) {
        this.removeDomNode();
      } else {
        this.createDomNode();
      }

      this.setState(state => ({ isVisible: !state.isVisible }));
    }
  };

  /** Create the dom node. */
  createDomNode() {
    this.domNode = document.createElement('div');
    this.domNode.className = classNames.portal;
    document.body.appendChild(this.domNode);
  }

  /** Remove the dom node from the dom. */
  removeDomNode() {
    if (this.domNode) {
      document.body.removeChild(this.domNode);
      this.domNode = null;
    }
  }

  /** @returns {object} JSX. */
  render() {
    return this.state.isVisible
      ? createPortal(
        <DevToolsUiContainer />,
        this.domNode,
      )
      : null;
  }
}
