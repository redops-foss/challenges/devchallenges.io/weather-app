import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { actionShape } from 'react/shapes/devtools';

import DevToolAction from '../action/Action';

import classNames from './Group.module.scss';

const DevToolGroup = ({
  group,
  actions,
}) => (
  <div className={classNames.group}>
    <h2 className={classNames.name}>{group}</h2>

    { actions.map(action => (
      <DevToolAction
        key={action.id}
        action={action}
      />
    )) }
  </div>
);

DevToolGroup.displayName = 'DevToolGroup';

DevToolGroup.propTypes = {
  group: PropTypes.string.isRequired,
  actions: PropTypes.arrayOf(actionShape).isRequired,
};

export default memo(DevToolGroup);
