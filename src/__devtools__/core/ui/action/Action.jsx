import React, { memo } from 'react';

import { actionShape } from 'react/shapes/devtools';
import { getName } from '../../manifest';

import classNames from './Action.module.scss';

const DevTool = ({
  action: {
    id,
    props,
    manifest: {
      name,
      render: DevToolComponent,
    },
  },
}) => (
  <div className={classNames.action}>
    <h3 className={classNames.name}>{getName(name, props)}</h3>

    <DevToolComponent
      devToolId={id}
      {...props}
    />
  </div>
);

DevTool.displayName = 'DevTool';

DevTool.propTypes = {
  action: actionShape.isRequired,
};

export default memo(DevTool);
