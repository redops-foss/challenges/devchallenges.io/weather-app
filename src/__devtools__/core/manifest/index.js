import { isFunction, noop, groupBy } from 'lodash';

export const getName = (name, props) => (
  isFunction(name)
    ? name(props)
    : name
);

export const getGroup = (group, props) => (
  isFunction(group)
    ? group(props)
    : group
);

export const makeManifest = ({
  name,
  render,
  mapProps,
  group,
}) => ({
  name,
  render,
  mapProps: mapProps || noop,
  group: group || 'Other',
});

export const groupByGroup = actions => groupBy(
  actions.map(action => ({
    ...action,
    manifest: {
      ...action.manifest,
      group: getGroup(action.manifest.group, action.props),
    },
  })),
  'manifest.group',
);
