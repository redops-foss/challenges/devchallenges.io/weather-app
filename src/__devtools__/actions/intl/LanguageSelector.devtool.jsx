import LanguageSelector from 'react/generic/intl/language-selector/LanguageSelector';
import { makeManifest, withRegisterDevTool } from '../../core/actions';

export default withRegisterDevTool(makeManifest({
  name: 'Language selector',
  render: LanguageSelector,
}));
