import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { getDefaultLocale } from 'services/intl';
import mockMessages from '__mockdata__/intl/locales/fr/messages';

import {
  SET_LOCALE_LOADING,
  SET_LOCALE_SUCCESS,
  SET_LOCALE_FAILURE,
  setLocale,
} from './actions';

import { ACTIONS } from './constants';

jest.mock('intl/locales/fr');
jest.mock('services/intl');

const mockStore = configureMockStore([thunk]);

describe('redux/intl/actions', () => {
  describe('SET_LOCALE_LOADING', () => {
    it('should return an action with the right schema', () => {
      expect(SET_LOCALE_LOADING('fr'))
        .toMatchObject({
          type: ACTIONS.SET_LOCALE_LOADING,
          payload: { locale: 'fr' },
        });
    });
  });

  describe('SET_LOCALE_FAILURE', () => {
    it('should return an action with the right schema', () => {
      const err = Error('TEST');
      expect(SET_LOCALE_FAILURE({
        locale: 'fr',
        err,
      }))
        .toMatchObject(
          {
            type: ACTIONS.SET_LOCALE_FAILURE,
            error: true,
            payload: {
              locale: 'fr',
              err,
            },
          },
        );
    });
  });

  describe('SET_LOCALE_SUCCESS', () => {
    it('should return an action with the right schema', () => {
      expect(SET_LOCALE_SUCCESS({
        locale: 'fr',
        messages: mockMessages,
      }))
        .toMatchObject(
          {
            type: ACTIONS.SET_LOCALE_SUCCESS,
            payload: {
              locale: 'fr',
              messages: mockMessages,
            },
          },
        );
    });
  });

  describe('getProjects', () => {
    it('should dispatch LOADING and SUCCESS', async () => {
      const store = mockStore({});

      await store.dispatch(setLocale('fr'));

      expect(store.getActions())
        .toEqual([
          SET_LOCALE_LOADING('fr'),
          SET_LOCALE_SUCCESS({
            locale: 'fr',
            messages: mockMessages,
          }),
        ]);
    });

    it('should use the default locale if not found', async () => {
      const store = mockStore({});

      getDefaultLocale.mockImplementationOnce(() => 'fr');

      await store.dispatch(setLocale());

      expect(store.getActions())
        .toEqual([
          SET_LOCALE_LOADING('fr'),
          SET_LOCALE_SUCCESS({
            locale: 'fr',
            messages: mockMessages,
          }),
        ]);
    });

    it('should dispatch LOADING and FAILURE', async () => {
      const store = mockStore({});

      await store.dispatch(setLocale('test'));

      expect(store.getActions().length)
        .toEqual(2);
      expect(store.getActions()[0])
        .toEqual(SET_LOCALE_LOADING('test'));
      expect(store.getActions()[1])
        .toMatchObject(SET_LOCALE_FAILURE({
          locale: 'test',
          err: {
            code: 'MODULE_NOT_FOUND',
          },
        }));
    });
  });
});
