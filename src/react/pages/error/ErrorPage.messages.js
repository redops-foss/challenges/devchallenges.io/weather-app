import { defineMessages } from 'react-intl';

export default defineMessages({
  OOPS: {
    defaultMessage: 'Oops.',
    description: 'Default error code',
  },

  DEFAULT_MESSAGE: {
    defaultMessage: 'Something went wrong',
    description: 'Default error message',
  },
});

export const errorMessages = defineMessages({
  404: {
    defaultMessage: 'Not Found',
    description: 'HTTP 404 error code',
  },

  500: {
    defaultMessage: 'Internal Server Error',
    description: 'HTTP 500 error code',
  },
});
