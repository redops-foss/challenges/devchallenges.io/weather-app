import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { FormattedMessage } from 'react-intl';

import messages, { errorMessages } from './ErrorPage.messages';

import classNames from './ErrorPage.module.scss';

export const ErrorPageBase = ({ code }) => (
  <div className={classNames.container}>
    <h1 className={classNames.code}>
      {code || <FormattedMessage {...messages.OOPS} />}
    </h1>

    <div className={classNames.description}>
      <FormattedMessage {...(errorMessages[code] || messages.DEFAULT_MESSAGE)} />
    </div>
  </div>
);

ErrorPageBase.displayName = 'ErrorPage';

ErrorPageBase.propTypes = {
  code: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

ErrorPageBase.defaultProps = {
  code: null,
};

export default memo(ErrorPageBase);
