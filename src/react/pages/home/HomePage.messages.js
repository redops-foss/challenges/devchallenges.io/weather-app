import { defineMessages } from 'react-intl';

export default defineMessages({
  PRIMARY_COLOR: {
    defaultMessage: 'Primary color',
  },

  SELECT_YOUR_LANGUAGE: {
    defaultMessage: 'Select your language:',
    description: 'This is the message above the language selector.',
  },

  LOAD_RANDOM_USER: {
    defaultMessage: 'Load random user',
  },

  DOCUMENTATION: {
    defaultMessage: 'Read the documentation.',
  },
});
