import React, { memo, useState, useCallback } from 'react';

import agent from 'services/http/api';
import { THEME } from 'config/style';

import { FormattedMessage } from 'react-intl';
import Logo from 'react/generic/logo/Logo';
import Avatar from 'react/generic/avatar/Avatar';
import Welcome from 'react/domain/home/welcome/Welcome';
import Button from 'react/generic/button/Button';
import LanguageSelector from 'react/generic/intl/language-selector/LanguageSelector';

import messages from './HomePage.messages';
import classNames from './HomePage.module.scss';

const sassVarStyle = { backgroundColor: THEME.primaryColor };

export const HomePageBase = () => {
  const [randomUser, onChangeRandomUser] = useState(null);

  const onLoadRandomUser = useCallback(
    async () => {
      const { body: { results: [user] } } = await agent.get('https://randomuser.me/api/');
      onChangeRandomUser(user);
    },
    [onChangeRandomUser],
  );

  return (
    <div className={classNames.home}>
      <main className={classNames.main}>
        <Logo className={classNames.logo} />

        <Welcome />
      </main>

      <div className={classNames.pageContainer}>
        <div className={classNames.pageContent}>
          {/* Theme. */}
          <div className={classNames.colorContainer}>
            <div
              style={sassVarStyle}
              className={classNames.color}
            />

            <FormattedMessage {...messages.PRIMARY_COLOR} />
          </div>

          {/* Language */}
          <div className={classNames.languageSelector}>
            <FormattedMessage {...messages.SELECT_YOUR_LANGUAGE} />
            <LanguageSelector />
          </div>

          {/* Random user */}
          <div className={classNames.userContainer}>
            <div>
              <Button onClick={onLoadRandomUser}>
                <FormattedMessage {...messages.LOAD_RANDOM_USER} />
              </Button>
            </div>

            { randomUser
              && (
                <div className={classNames.user}>
                  <Avatar
                    size={60}
                    src={randomUser.picture.large}
                  />

                  <div className={classNames.name}>
                    <div className={classNames.firstName}>{randomUser.name.first}</div>
                    <div className={classNames.lastName}>{randomUser.name.last}</div>
                  </div>
                </div>
              )}
          </div>

          {/* Documentation. */}
          <div className={classNames.documentation}>
            <div>
              <a
                href="https://innersource.soprasteria.com/redops/resources/boilerplates/front-react-boilerplate/-/wikis/home"
                target="_blank"
                rel="noreferrer"
              >
                <FormattedMessage {...messages.DOCUMENTATION} />
              </a>
            </div>

            { __DEV_TOOLS_ENABLED__
              && (
                <div>Press ctrl+² to display the dev tools panel.</div>
              ) }
          </div>
        </div>
      </div>
    </div>
  );
};

HomePageBase.displayName = 'HomePage';

export default memo(HomePageBase);
