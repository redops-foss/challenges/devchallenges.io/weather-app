import React, { memo } from 'react';
import { string, node } from 'prop-types';
import cn from 'classnames';

import classNames from './IconButton.module.scss';

export const IconButtonBase = ({
  className,
  label,
}) => (
  <button className={cn(className, classNames.button)}>
    { label }
  </button>
);

IconButtonBase.displayName = 'IconButton';

IconButtonBase.propTypes = {
  /** Add your own style on the component. */
  className: string,
  /** Inside the icon button. */
  label: node,
};

IconButtonBase.defaultProps = {
  className: '',
  label: null,
};

export default memo(IconButtonBase);
