import React, {
  memo,
  lazy,
  Suspense,
  useMemo,
} from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';

import ImpulseSpinner from 'react/generic/loading/impulse-spinner/ImpulseSpinner';

import classNames from './Asset.module.scss';

const ICON_FILES = {
  LOGO: 'logo/logo.svg',
};

const ASSET_ICONS = Object.keys(ICON_FILES)
  .reduce(
    (iconMap, name) => {
      iconMap[name] = name;
      return iconMap;
    },
    {},
  );

export const AssetBase = ({
  className,
  name,
  primaryColor,
  secondaryColor,
  ...props
}) => {
  const ImportedAsset = useMemo(
    () => lazy(() => import(`style/assets/${ICON_FILES[name]}`)),
    [name],
  );
  return (
    <Suspense fallback={<ImpulseSpinner />}>
      <ImportedAsset
        {...props}
        className={cn(className, classNames.asset)}
        style={{
          color: primaryColor,
          fill: secondaryColor || primaryColor,
        }}
      />
    </Suspense>
  );
};

AssetBase.displayName = 'AssetBase';

AssetBase.propTypes = {
  /** ClassName. */
  className: PropTypes.string,
  /** Asset name -> use Asset.ICONS.NAME. */
  name: PropTypes.oneOf(Object.values(ASSET_ICONS)),
  /** Asset primary color. */
  primaryColor: PropTypes.string,
  /** Asset secondary color. */
  secondaryColor: PropTypes.string,
};

AssetBase.defaultProps = {
  className: null,
  name: ASSET_ICONS.LOGO,
  primaryColor: null,
  secondaryColor: null,
};

const Asset = memo(AssetBase);

Asset.ICONS = ASSET_ICONS;

export default Asset;
