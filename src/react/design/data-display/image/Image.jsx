import React, { memo, useCallback } from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';

import ImpulseSpinner from 'react/generic/loading/impulse-spinner/ImpulseSpinner';
import useIcon from 'react/shared/hooks/useIcon.hook';

import classNames from './Image.module.scss';

const ICON_FILES = {
  WEATHER_CLEAR: 'weather/Clear.png',
  WEATHER_CLOUD_BACKGROUND: 'weather/Cloud-background.png',
  WEATHER_HAIL: 'weather/Hail.png',
  WEATHER_HEAVY_CLOUD: 'weather/HeavyCloud.png',
  WEATHER_HEAVY_RAIN: 'weather/HeavyRain.png',
  WEATHER_LIGHT_CLOUD: 'weather/LightCloud.png',
  WEATHER_LIGHT_RAIN: 'weather/LightRain.png',
  WEATHER_SHOWER: 'weather/Shower.png',
  WEATHER_SLEET: 'weather/Sleet.png',
  WEATHER_SNOW: 'weather/Snow.png',
  WEATHER_THUNDERSTORM: 'weather/Thunderstorm.png',
};

const ASSET_IMAGES = Object.keys(ICON_FILES)
  .reduce(
    (iconMap, name) => {
      iconMap[name] = name;
      return iconMap;
    },
    {},
  );

export const ImageBase = ({
  className,
  name,
  primaryColor,
  secondaryColor,
  ...props
}) => {
  const importAsset = useCallback(
    () => import(`style/assets/${ICON_FILES[name]}`),
    [name],
  );

  const { url, isLoading } = useIcon(importAsset);

  return (
    isLoading || !url
      ? <ImpulseSpinner />
      : <img src={url} alt="a cloud lol" className={cn(className, classNames.asset)} {...props} />
  );
};

ImageBase.displayName = 'Image';

ImageBase.propTypes = {
  /** ClassName. */
  className: PropTypes.string,
  /** Image name -> use Image.ASSETS.NAME. */
  name: PropTypes.oneOf(Object.values(ASSET_IMAGES)),
  /** Image primary color. */
  primaryColor: PropTypes.string,
  /** Image secondary color. */
  secondaryColor: PropTypes.string,
};

ImageBase.defaultProps = {
  className: null,
  name: ASSET_IMAGES.WEATHER_CLEAR,
  primaryColor: null,
  secondaryColor: null,
};

const Image = memo(ImageBase);

Image.ASSETS = ASSET_IMAGES;

export default Image;
