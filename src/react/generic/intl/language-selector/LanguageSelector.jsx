import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';

import connect from 'utils/redux/connect';
import { selectLocale } from 'redux/intl/selectors';
import { setLocale } from 'redux/intl/actions';

const enhancer = compose(
  connect(
    state => ({
      value: selectLocale(state),
    }),
    { onChange: event => setLocale(event.target.value) },
  ),

  memo,
);

const LanguageSelector = ({
  value,
  onChange,
}) => (
  <select value={value} onChange={onChange}>
    <option value="en">English</option>
    <option value="fr">French</option>
    <option value="es">Spanish</option>
  </select>
);

LanguageSelector.displayName = 'LanguageSelector';

LanguageSelector.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default enhancer(LanguageSelector);
