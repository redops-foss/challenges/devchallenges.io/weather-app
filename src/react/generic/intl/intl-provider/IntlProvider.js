import { IntlProvider as ReactIntlProvider } from 'react-intl';

import { selectLocaleBundle } from 'redux/intl/selectors';
import connect from 'utils/redux/connect';

export default connect(selectLocaleBundle)(ReactIntlProvider);
