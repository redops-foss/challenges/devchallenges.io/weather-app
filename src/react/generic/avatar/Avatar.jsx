import React, { memo, useMemo } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import classNames from './Avatar.module.scss';

export const AvatarBase = ({
  src,
  size,
  style,
  className,
  ...props
}) => {
  const computedStyle = useMemo(
    () => ({
      ...style,
      height: size,
      width: size,
      backgroundImage: `url(${src})`,
    }),
    [src, size, style],
  );

  return (
    <div
      {...props}
      style={computedStyle}
      className={cn(className, classNames.avatar)}
    />
  );
};

AvatarBase.displayName = 'Avatar';

AvatarBase.propTypes = {
  src: PropTypes.string.isRequired,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  style: PropTypes.shape({}),
  className: PropTypes.string,
};

AvatarBase.defaultProps = {
  size: 80,
  style: null,
  className: null,
};

export default memo(AvatarBase);
