import React, { memo } from 'react';

import LogoSvg from 'style/assets/logo/logo.svg';

const LogoBase = props => (
  <LogoSvg {...props} />
);

LogoBase.displayName = 'Logo';

export default memo(LogoBase);
