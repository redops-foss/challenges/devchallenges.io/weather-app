import React, { memo } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import classNames from './Button.module.scss';

export const ButtonBase = ({
  className,
  ...props
}) => (
  <button
    {...props}
    className={cn(className, classNames.button)}
  />
);

ButtonBase.displayName = 'Button';

ButtonBase.propTypes = {
  /** Root element className. */
  className: PropTypes.string,
  /** Button a11y type. */
  type: PropTypes.oneOf([
    'submit',
    'reset',
    'button',
  ]),
};

ButtonBase.defaultProps = {
  className: null,
  type: 'button',
};

export default memo(ButtonBase);
