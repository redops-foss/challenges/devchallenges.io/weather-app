import { omit } from 'lodash/fp';
import { mapProps } from 'recompose';

export default propsToOmit => mapProps(omit(propsToOmit));
