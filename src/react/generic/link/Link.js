import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { elementShape } from 'react/shapes/vendor';

import { Link as ReactRouterLink } from 'react-router-dom';

/* A Link that can be disabled. */
export const LinkBase = ({
  isDisabled,
  disabledElement,
  children,
  ...props
}) => React.createElement(
  isDisabled ? disabledElement : ReactRouterLink,
  props,
  children,
);

LinkBase.displayName = 'LinkProxy';

LinkBase.propTypes = {
  isDisabled: PropTypes.bool,
  /** HTML element instead of `<a>` when the link is disabled. */
  disabledElement: elementShape,
  children: PropTypes.node,
};

LinkBase.defaultProps = {
  isDisabled: false,
  disabledElement: 'div',
  children: null,
};

export default memo(LinkBase);
