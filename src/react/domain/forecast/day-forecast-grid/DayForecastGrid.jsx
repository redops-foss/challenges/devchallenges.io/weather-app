import React, { memo } from 'react';
import { node } from 'prop-types';

import classNames from './DayForecastGrid.module.scss';

export const DayForecastGridBase = ({ children }) => (
  <div className={classNames.grid}>{ children }</div>
);

DayForecastGridBase.displayName = 'DayForecastGrid';
DayForecastGridBase.propTypes = {
  children: node,
};
DayForecastGridBase.defaultProps = {
  children: null,
};

export default memo(DayForecastGridBase);
