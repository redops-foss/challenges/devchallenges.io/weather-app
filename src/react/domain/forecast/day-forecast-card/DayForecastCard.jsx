import React, { memo } from 'react';
import { number, oneOf } from 'prop-types';
import dayjs from 'dayjs';

import Image from 'react/design/data-display/image/Image';
import { dateShape } from 'react/shapes/vendor';

import { TEMPERATURE_UNITS, WEATHER_ICONS } from 'domain/weather/constants';

import classNames from './DayForecastCard.module.scss';

const OTHER_DAYS = 'ddd, D MMM';

const DAYSJS_CALENDAR_OPTIONS = {
  sameDay: '[Today]',
  nextDay: '[Tomorrow]',
  lastDay: '[Yesterday]',
  nextWeek: OTHER_DAYS,
  lastWeek: OTHER_DAYS,
  sameElse: OTHER_DAYS,
};

export const DayForecastCardBase = ({
  date,
  weather,
  temperatureMin,
  temperatureMax,
  temperatureUnit,
}) => (
  <div className={classNames.container}>
    <span className={classNames.date}>
      {dayjs(date).calendar(dayjs(), DAYSJS_CALENDAR_OPTIONS)}
    </span>
    <div className={classNames.iconContainer}>
      <Image className={classNames.icon} name={Image.ASSETS[weather]} />
    </div>
    <div className={classNames.maxMinContainer}>
      <span className={classNames.max}>{temperatureMax}{temperatureUnit}</span>
      <span className={classNames.min}>{temperatureMin}{temperatureUnit}</span>
    </div>
  </div>
);

DayForecastCardBase.displayName = 'DayForecastCard';
DayForecastCardBase.propTypes = {
  date: dateShape,
  weather: oneOf(Object.values(WEATHER_ICONS)),
  temperatureMin: number,
  temperatureMax: number,
  temperatureUnit: oneOf(Object.values(TEMPERATURE_UNITS)),
};
DayForecastCardBase.defaultProps = {
  date: null,
  weather: WEATHER_ICONS.CLEAR,
  temperatureMin: 0,
  temperatureMax: 0,
  temperatureUnit: TEMPERATURE_UNITS.CELSIUS,
};

export default memo(DayForecastCardBase);
