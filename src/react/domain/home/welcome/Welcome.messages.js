import { defineMessages } from 'react-intl';

export default defineMessages({
  REDOPS: {
    defaultMessage: 'RedOps',
    description: 'App title',
  },

  WELCOME: {
    defaultMessage: 'React boilerplate',
    description: 'App subtitle',
  },
});
