import React, { memo } from 'react';

import { FormattedMessage } from 'react-intl';

import messages from './Welcome.messages';
import classNames from './Welcome.module.scss';

const Welcome = () => (
  <div className={classNames.container}>
    <h1 className={classNames.redops}>
      <FormattedMessage {...messages.REDOPS} />
    </h1>
    <h2 className={classNames.welcome}>
      <FormattedMessage {...messages.WELCOME} />
    </h2>
  </div>
);

Welcome.displayName = 'Welcome';

export default memo(Welcome);
