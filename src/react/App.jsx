import React, { Suspense, lazy } from 'react';
import { lifecycle, compose } from 'recompose';

import { HelmetProvider } from 'react-helmet-async';
import { Provider as ReduxProvider } from 'react-redux';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';

import { QueryCache, ReactQueryCacheProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query-devtools';

import store from 'redux/store';
import { hide as hideSplashScreen } from 'services/splash';

import { DevToolsProvider, DevToolsUi, LoadDevTool } from '__devtools__';

import IntlProvider from './generic/intl/intl-provider/IntlProvider';
import LoadingScreen from './generic/loading/loading-screen/LoadingScreen';

import routes from './routes';

const Home = lazy(() => import('./views/home/HomeView'));
const NotFound = lazy(() => import('./views/error/not-found/NotFoundView'));

const enhancer = compose(
  lifecycle({
    /** Hide splash screen when app is mounted. */
    componentDidMount() {
      hideSplashScreen();
    },
  }),
);

const cache = new QueryCache({
  defaultConfig: {
    queries: {
      retry: 0,
      suspense: true,
      staleTime: 0,
    },
  },
});

const App = () => (
  <ReactQueryCacheProvider queryCache={cache}>
    <ReactQueryDevtools />
    <HelmetProvider>
      <ReduxProvider store={store}>
        <IntlProvider>
          <Router>
            <Suspense fallback={<LoadingScreen />}>
              <DevToolsProvider>
                <Switch>
                  <Route
                    path={routes.HOME.ROOT}
                    exact
                    component={Home}
                  />

                  <Route component={NotFound} />

                </Switch>
                <DevToolsUi />
                <LoadDevTool name="intl/LanguageSelector" />
              </DevToolsProvider>
            </Suspense>
          </Router>
        </IntlProvider>
      </ReduxProvider>
    </HelmetProvider>
  </ReactQueryCacheProvider>
);

export default enhancer(App);
