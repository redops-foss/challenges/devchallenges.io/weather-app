import {
  instanceOf, number, oneOfType, string, func, shape,
} from 'prop-types';

export const elementShape = oneOfType([
  // Class component or functional component without memo / forwardRef.
  func,
  // Function component (with memo or forwardRef).
  shape({}),
  // Native element name.
  string,
]);

export const dateShape = oneOfType([instanceOf(Date), string, number]);
