import { useEffect, useRef, useState } from 'react';

export default (importAsset) => {
  const ImportedImageRef = useRef();
  const [isLoading, setLoading] = useState(false);
  const { current: url } = ImportedImageRef;

  useEffect(
    () => {
      (async () => {
        setLoading(true);

        try {
          ImportedImageRef.current = (await importAsset()).default;
        } finally {
          setLoading(false);
        }
      })();
    },
    [importAsset],
  );

  return { url, isLoading };
};
