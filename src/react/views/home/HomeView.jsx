import React, { memo } from 'react';

import { Helmet } from 'react-helmet-async';
import FormattedMessageChildren
  from 'react/generic/intl/formatted-message-children/FormattedMessageChildren';
import HomePage from 'react/pages/home/HomePage';

import messages from './HomeView.messages';

const HomeViewBase = () => (
  <>
    {/* Set the page title. */}
    <FormattedMessageChildren {...messages.PAGE_TITLE}>
      {title => (
        <Helmet>
          <title>{title}</title>
        </Helmet>
      )}
    </FormattedMessageChildren>
    <HomePage />
  </>
);

HomeViewBase.displayName = 'HomeView';

export default memo(HomeViewBase);
