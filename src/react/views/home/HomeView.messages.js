import { defineMessages } from 'react-intl';

export default defineMessages({
  PAGE_TITLE: {
    defaultMessage: 'Home | RedOps boilerplate',
  },
});
