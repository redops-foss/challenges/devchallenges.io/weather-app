import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet-async';

import FormattedMessageChildren from 'react/generic/intl/formatted-message-children/FormattedMessageChildren';
import ErrorPage from 'react/pages/error/ErrorPage';

import messages from './ErrorView.messages';

const ErrorView = ({
  code,
}) => (
  <>
    <FormattedMessageChildren
      {...messages.PAGE_TITLE}
      values={{
        code,
        hasCode: code && 'true',
      }}
    >
      {title => (
        <Helmet>
          <title>{title}</title>
        </Helmet>
      )}
    </FormattedMessageChildren>

    <ErrorPage code={code} />
  </>
);

ErrorView.displayName = 'ErrorView';

ErrorView.propTypes = {
  code: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

ErrorView.defaultProps = {
  code: null,
};

export default memo(ErrorView);
