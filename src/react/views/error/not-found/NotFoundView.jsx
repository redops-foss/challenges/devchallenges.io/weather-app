import { withProps } from 'recompose';

import ErrorView from '../ErrorView';

export default withProps(() => ({ code: '404' }))(ErrorView);
