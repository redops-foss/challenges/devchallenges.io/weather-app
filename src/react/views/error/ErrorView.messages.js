import { defineMessages } from 'react-intl';

export default defineMessages({
  PAGE_TITLE: {
    defaultMessage: '{hasCode, select, true {{code}} other {Error}} | RedOps boilerplate',
    description: "`code` is the error code, and `hasCode` is 'true' if `code` is defined.",
  },
});
