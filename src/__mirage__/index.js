import { Server } from 'miragejs';

import loadRoutes from './routes';

export const startServer = ({ environment = 'development', trackRequests = false } = {}) => (
  new Server({
    /*
     * 'development' or 'test'
     *
     * @see {@link https://miragejs.com/docs/testing/application-tests/#the-test-environment}
     */
    environment,

    /**
     * Allow to track requests on the server in case
     * you need to assert against the request you sent.
     *
     * @see {@link https://miragejs.com/docs/testing/assertions/#asserting-against-handled-requests-and-responses.}
     */
    trackRequests,

    /**
     * Load your routes.
     * I extracted it to show you that you should extract
     * a lot of configuration in case it became too heavy.
     */
    routes() {
      loadRoutes(this);
    },
  })
);
