import searchMockData from 'domain/weather/__mockdata__/location';
import forecastMockData from 'domain/weather/__mockdata__/forecast';

export default (app) => {
  app.get('/api/location/search', () => searchMockData);
  app.get('/api/location/:id', () => forecastMockData);

  app.passthrough();
  app.passthrough('https://randomuser.me/api/**');
};
