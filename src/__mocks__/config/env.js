export default {
  app: {
    name: '@mock/app-name',
    version: '1.0.0',
  },

  api: {
    endpoint: 'http://api.endpoint/',
  },
};
