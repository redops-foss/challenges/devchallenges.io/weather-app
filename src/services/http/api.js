import superagent from 'superagent';

import withApiPrefix from 'services/api/withApiPrefix.middleware';

const agent = superagent.agent();

agent.use(withApiPrefix);

export default agent;
