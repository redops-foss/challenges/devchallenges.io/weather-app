import agent from './api';

jest.mock('config/env');

describe('services/http/api', () => {
  describe('API agent', () => {
    it('should send requests to the API endpoint when the url starts with slash', () => {
      const request = agent.get('/api/v1/yolo');
      request.end(() => { });
      expect(request.url).toBe('http://api.endpoint/api/v1/yolo');
    });

    it('should not send requests to the API endpoint when the url does not start with slash', () => {
      const request = agent.get('http://not.our.api.endpoint/api/v1/yolo');
      request.end(() => { });
      expect(request.url).toBe('http://not.our.api.endpoint/api/v1/yolo');
    });
  });
});
