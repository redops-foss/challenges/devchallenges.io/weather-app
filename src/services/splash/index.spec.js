import { hide, show } from './index';

describe('services/splash', () => {
  describe('hide', () => {
    it('should remove or add classnames to the elements', async () => {
      const splash = document.createElement('splash');
      const body = document.createElement('body');

      body.classList.add('body--has-splash');

      jest.spyOn(document, 'getElementById').mockReturnValue(splash);
      jest.spyOn(document, 'getElementsByTagName').mockReturnValue([body]);

      await hide();

      expect(splash.classList.contains('splash--fading')).toBe(true);
      expect(splash.classList.contains('splash--hidden')).toBe(true);
      expect(body.classList.contains('body--has-splash')).toBe(false);
    });
  });

  describe('show', () => {
    it('should remove or add classnames to the elements', async () => {
      const splash = document.createElement('splash');
      const body = document.createElement('body');

      splash.classList.add('splash--fading');
      splash.classList.add('splash--hidden');

      jest.spyOn(document, 'getElementById').mockReturnValue(splash);
      jest.spyOn(document, 'getElementsByTagName').mockReturnValue([body]);

      await show();

      expect(splash.classList.contains('splash--fading')).toBe(false);
      expect(splash.classList.contains('splash--hidden')).toBe(false);
      expect(body.classList.contains('body--has-splash')).toBe(true);
    });
  });
});
