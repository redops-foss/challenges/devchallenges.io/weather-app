import dayjs from 'dayjs';
import 'dayjs/locale/en-gb';

describe('services/dayjs/bootstrap', () => {
  it('should be bootstrapped during setup', () => {
    dayjs.locale('en-gb');
    expect(dayjs('2020-09-02').format('L')).toBe('02/09/2020');
    expect(dayjs('2020-09-02').format('LL')).toBe('2 September 2020');
  });
});
