import calendar from 'dayjs/plugin/calendar';
import localizedFormatPlugin from 'dayjs/plugin/localizedFormat';

export default (dayjs) => {
  // Dayjs offers opt-in plugins to enhance its API.
  // https://day.js.org/docs/en/plugin/plugin

  // Add support for 'L', 'LL', 'LT', etc. formats.
  dayjs.extend(localizedFormatPlugin);
  dayjs.extend(calendar);
};
