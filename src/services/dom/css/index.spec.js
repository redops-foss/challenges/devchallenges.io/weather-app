import {
  classes, addClassname, removeClassname, removeClassnames,
} from './index';

describe('services/dom/css', () => {
  /*
   * In order to get into the branches where classList is not supported,
   * and because delete element.classList does not work,
   * I clone the element while omitting this property.
   */
  const createElementWithoutClassList = (...args) => {
    const element = document.createElement(...args);
    const { classList, ...clone } = element;
    return clone;
  };

  describe('createElementWithoutClassList', () => {
    it('should not have a classList property', () => {
      const element = createElementWithoutClassList('any');
      expect(element.classList).toBeUndefined();
    });
  });

  describe('classes', () => {
    it('should return an empty array if given NodeElement has no classes', () => {
      const element = document.createElement('any');
      expect(classes(element)).toMatchObject([]);
    });

    it('should return an array of all classes defined on the element', () => {
      const element = document.createElement('any');
      const classList = ['justin', 'bieber', 'ariana', 'grande'];
      element.classList.add(...classList);
      expect(classes(element)).toMatchObject(classList);
    });
  });

  describe('addClassname', () => {
    describe('Not IE', () => {
      it('should add a className on an element if not already present', () => {
        const element = document.createElement('any');
        const className = 'justin';
        expect(addClassname(element, className)).toBe(className);
        expect(classes(element).includes(className)).toBe(true);
      });

      it('should not add a className on an element if already present', () => {
        const element = document.createElement('any');
        const className = 'justin';
        addClassname(element, className);
        expect(addClassname(element, className)).toBe(className);
        expect(classes(element).length).toBe(1);
      });
    });

    describe('IE', () => {
      it('should add a className on an element if not already present', () => {
        const element = createElementWithoutClassList('any');
        const className = 'justin';

        expect(addClassname(element, className)).toBe(className);
        expect(classes(element).includes(className)).toBe(true);
      });

      it('should not add a className on an element if already present', () => {
        const element = createElementWithoutClassList('any');
        const className = 'justin';

        addClassname(element, className);
        expect(addClassname(element, className)).toBe(className);
        expect(classes(element).length).toBe(1);
      });

      it('should not remove other classNames on an element if already present', () => {
        const element = createElementWithoutClassList('any');
        const className = 'justin';

        addClassname(element, 'ariana');
        addClassname(element, 'grande');
        expect(addClassname(element, className)).toBe(`ariana grande ${className}`);
        expect(classes(element).length).toBe(3);
      });
    });
  });

  describe('removeClassname', () => {
    describe('Not IE', () => {
      it('should not remove a className on an element if not already present', () => {
        const element = document.createElement('any');
        const className = 'justin';
        expect(removeClassname(element, className)).toBe('');
        expect(classes(element).length).toBe(0);
      });

      it('should not remove a className on an element if not already present', () => {
        const element = document.createElement('any');
        const className = 'justin';

        addClassname(element, 'ariana');
        addClassname(element, 'grande');

        expect(removeClassname(element, className)).toBe('ariana grande');
        expect(classes(element).length).toBe(2);
      });

      it('should remove a className on an element if already present', () => {
        const element = document.createElement('any');
        const className = 'justin';

        addClassname(element, 'ariana');
        addClassname(element, className);
        addClassname(element, 'grande');

        expect(removeClassname(element, className)).toBe('ariana grande');
        expect(classes(element).length).toBe(2);
      });
    });

    describe('IE', () => {
      it('should not remove a className on an element if not already present', () => {
        const element = createElementWithoutClassList('any');
        const className = 'justin';

        expect(removeClassname(element, className)).toBe('');
        expect(classes(element).length).toBe(0);
      });

      it('should not remove a className on an element if not already present', () => {
        const element = createElementWithoutClassList('any');
        const className = 'justin';

        addClassname(element, 'ariana');
        addClassname(element, 'grande');

        expect(removeClassname(element, className)).toBe('ariana grande');
        expect(classes(element).length).toBe(2);
      });

      it('should remove a className on an element if already present', () => {
        const element = createElementWithoutClassList('any');
        const className = 'justin';

        addClassname(element, 'ariana');
        addClassname(element, className);
        addClassname(element, 'grande');

        expect(removeClassname(element, className)).toBe('ariana grande');
        expect(classes(element).length).toBe(2);
      });
    });
  });

  describe('removeClassnames', () => {
    describe('Not IE', () => {
      it('should not remove classNames on an element if not already present', () => {
        const element = document.createElement('any');
        const className = 'justin bieber';
        expect(removeClassnames(element, className)).toBe('');
        expect(classes(element).length).toBe(0);
      });

      it('should not remove classNames on an element if not already present', () => {
        const element = document.createElement('any');
        const className = 'justin bieber';

        addClassname(element, 'ariana');
        addClassname(element, 'grande');

        expect(removeClassnames(element, className)).toBe('ariana grande');
        expect(classes(element).length).toBe(2);
      });

      it('should remove a className on an element if already present', () => {
        const element = document.createElement('any');
        const className = 'justin bieber';

        addClassname(element, 'ariana');
        addClassname(element, 'bieber');
        addClassname(element, 'grande');

        expect(removeClassnames(element, className)).toBe('ariana grande');
        expect(classes(element).length).toBe(2);
      });

      it('should remove a className on an element if already present', () => {
        const element = document.createElement('any');
        const className = 'justin bieber';

        addClassname(element, 'ariana');
        addClassname(element, 'justin');
        addClassname(element, 'grande');

        expect(removeClassnames(element, className)).toBe('ariana grande');
        expect(classes(element).length).toBe(2);
      });

      it('should remove all classNames on an element if already present', () => {
        const element = document.createElement('any');
        const className = 'justin bieber';

        addClassname(element, 'ariana');
        addClassname(element, 'justin');
        addClassname(element, 'bieber');
        addClassname(element, 'grande');

        expect(removeClassnames(element, className)).toBe('ariana grande');
        expect(classes(element).length).toBe(2);
      });
    });

    describe('IE', () => {
      it('should not remove classNames on an element if not already present', () => {
        const element = createElementWithoutClassList('any');
        const className = 'justin bieber';

        expect(removeClassnames(element, className)).toBe('');
        expect(classes(element).length).toBe(0);
      });

      it('should not remove classNames on an element if not already present', () => {
        const element = createElementWithoutClassList('any');
        const className = 'justin bieber';

        addClassname(element, 'ariana');
        addClassname(element, 'grande');

        expect(removeClassnames(element, className)).toBe('ariana grande');
        expect(classes(element).length).toBe(2);
      });

      it('should remove a className on an element if already present', () => {
        const element = createElementWithoutClassList('any');
        const className = 'justin bieber';

        addClassname(element, 'ariana');
        addClassname(element, 'bieber');
        addClassname(element, 'grande');

        expect(removeClassnames(element, className)).toBe('ariana grande');
        expect(classes(element).length).toBe(2);
      });

      it('should remove a className on an element if already present', () => {
        const element = createElementWithoutClassList('any');
        const className = 'justin bieber';

        addClassname(element, 'ariana');
        addClassname(element, 'justin');
        addClassname(element, 'grande');

        expect(removeClassnames(element, className)).toBe('ariana grande');
        expect(classes(element).length).toBe(2);
      });

      it('should remove all classNames on an element if already present', () => {
        const element = createElementWithoutClassList('any');
        const className = 'justin bieber';

        addClassname(element, 'ariana');
        addClassname(element, 'justin');
        addClassname(element, 'justin bieber');
        addClassname(element, 'grande');

        expect(removeClassnames(element, className)).toBe('ariana grande');
        expect(classes(element).length).toBe(2);
      });
    });
  });
});
