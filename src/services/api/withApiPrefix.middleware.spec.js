import config from 'config/env';
import { removeTrailingSlash } from 'utils/string';

import withApiPrefix from './withApiPrefix.middleware';

jest.mock('config/env');

const endpoint = removeTrailingSlash(config.api.endpoint);

describe('services/api/withApiPrefix', () => {
  it('should prefix requests url with the api url if it starts with slash', () => {
    const url = '/justin/bieber';
    expect(withApiPrefix({ url })).toMatchObject({ url: `${endpoint}${url}` });
  });

  it('should not prefix requests url with the api url does not start with slash', () => {
    expect(withApiPrefix()).toBeUndefined();
    expect(withApiPrefix({})).toMatchObject({});
    expect(withApiPrefix({ url: '' })).toMatchObject({ url: '' });
    expect(withApiPrefix({ url: 'http://not.a.slash' })).toMatchObject({ url: 'http://not.a.slash' });
  });
});
