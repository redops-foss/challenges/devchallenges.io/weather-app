import React from 'react';

import useLocation from '../hooks/useLocation.hook';
import useWeather from '../hooks/useWeather.hook';

const WithWeatherHOC = WrappedComponent => ({ ...props }) => {
  const {
    data: location,
    isFetching: isFetchingLocation,
  } = useLocation('Lille');
  const {
    data: weather,
    isFetching: isFetchingWeather,
  } = useWeather(location);

  if (isFetchingLocation || isFetchingWeather) {
    return <>Loading</>;
  }

  if (weather === undefined) {
    return <>No data</>;
  }

  return (
    <WrappedComponent
      temperature={weather.temperature}
      {...props}
    />
  );
};

export default WithWeatherHOC;
