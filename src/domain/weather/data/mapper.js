export const mapWeatherFromApi = (
  {
    consolidated_weather: [{
      // eslint-disable-next-line camelcase
      the_temp,
    }],
  },
) => ({
  temperature: the_temp,
});
