import client from 'domain/weather/services/http/client';
import { mapWeatherFromApi } from './mapper';

export const fetchLocationId = async (query) => {
  const { body: [location] } = await client.get('/api/location/search').query({ query });
  return location.woeid;
};

export const fetchWeather = async (id) => {
  const { body } = await client.get(`/api/location/${id}`);
  return mapWeatherFromApi(body);
};

