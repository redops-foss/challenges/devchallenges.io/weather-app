export default [
  {
    title: 'Lille',
    location_type: 'City',
    woeid: 608105,
    latt_long: '50.637150,3.062830',
  },
];
