import { useQuery } from 'react-query';
import { fetchWeather } from 'domain/weather/data/repository';

export default id => useQuery(
  ['weather', id],
  () => fetchWeather(id),
  { enabled: id },
);
