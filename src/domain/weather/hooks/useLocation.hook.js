import { useQuery } from 'react-query';
import { fetchLocationId } from 'domain/weather/data/repository';

export default location => useQuery(
  ['location', location],
  () => fetchLocationId(location),
);
