const { transform } = require('lodash');
const { parse } = require('intl-messageformat-parser');

/**
 * Parse react-intl messages at build time.
 *
 * @param {object} source - Map of messages. ID -> message.
 * @returns {object} ID -> parsed message.
 */
module.exports = function compileReactIntlMessagesLoader(source) {
  // Set the loader as cacheable (no external dependencies).
  this.cacheable();

  // Parse the messages to AST format for react-intl.
  return transform(
    source,
    (acc, msg, id) => {
      if (id && msg) {
        acc[id] = parse(msg);
      }
    },
    {},
  );
};
