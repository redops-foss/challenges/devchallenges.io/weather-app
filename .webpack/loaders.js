const fs = require('fs');
const path = require('path');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const pixrem = require('pixrem');

const babelConfig = JSON.parse(fs.readFileSync(
  path.resolve(__dirname, '../.babelrc'),
  { encoding: 'utf8' },
));

// This loader will use the default babel behavior of discovering babel configuration files.
const babelLoader = [
  {
    loader: 'babel-loader',
  },
];

// This loader enforces our root babel configuration. This is useful to transpile node_modules.
const rootConfigBabelLoader = [
  {
    loader: 'babel-loader',
    options: {
      ...babelConfig,
    },
  },
];

const eslintLoader = [
  {
    loader: 'eslint-loader',
    options: {
      // Do not stop the build on a lint error.
      emitWarning: true,
    },
  },
];

const makeCssLoader = ({
  mode = 'development',
  enableModules = false,
  importLoaders = 0,
} = {}) => ([
  {
    loader: mode === 'production'
      // In production, extract css to an external stylesheet to allow caching.
      ? MiniCssExtractPlugin.loader
      // In development, use the style-loader (faster and allows MHR).
      : 'style-loader',

    options: {
      esModule: true,
    },
  },
  {
    loader: 'css-loader',
    options: {
      sourceMap: true,
      importLoaders: importLoaders + 1,
      modules: enableModules
        ? {
          localIdentName: mode === 'production'
            // In production, only set the hash of the class name.
            ? '[hash:base64:8]'
            // In development, add the actual class name to the hash to make it easier to debug.
            : '[local]--[hash:base64:8]',
        }
        : undefined,
    },
  },
  {
    loader: 'postcss-loader',
    options: {
      sourceMap: true,
      postcssOptions: {
        plugins: [
          pixrem(),
        ],
      },
    },
  },
]);

const makeSassLoader = ({
  mode,
  enableModules,
  importLoaders = 0,
} = {}) => ([
  ...makeCssLoader({
    enableModules,
    mode,
    importLoaders: importLoaders + 2,
  }),
  {
    // This loader will resolve URLs relative to the source file.
    // (otherwise they will be relative to the built destination file and URLs will not work).
    loader: 'resolve-url-loader',
    options: {
      sourceMap: true,
    },
  },
  {
    loader: 'sass-loader',
    options: {
      sourceMap: true,
      sassOptions: {
        includePaths: [
          path.resolve(__dirname, '../src/'),
          path.resolve(__dirname, '../node_modules/'),
        ],
      },
    },
  },
]);

const sassVariablesLoader = [
  {
    loader: 'sass-extract-loader',
    options: {
      plugins: ['sass-extract-js'],
      includePaths: [
        path.resolve(__dirname, '../src/'),
        path.resolve(__dirname, '../node_modules/'),
      ],
    },
  },
];

const makeAssetsLoader = ({ mode } = {}) => [
  {
    loader: 'file-loader',
    options: {
      name: mode === 'production'
        ? 'static/[hash:8].[ext]'
        : 'static/[name].[hash:8].[ext]',
    },
  },
];

const svgReactLoader = [
  {
    // Export a React component.
    loader: 'svg-react-loader',
  },
  {
    loader: 'image-webpack-loader',
    options: {
      // Optimize svg files.
      svgo: {
        plugins: [
          // Keep the viewbox and remove the hardcoded dimensions
          // to be able to set the dimensions via css.
          { removeViewBox: false },
          { removeDimensions: true },

          // Removing ids breaks some svgs.
          { cleanupIDs: false },
        ],
      },
    },
  },
];

const poLoader = [
  {
    loader: 'json-loader',
  },
  {
    // Compile messages to AST for react-intl.
    loader: path.resolve(__dirname, './intl/compileReactIntlMessagesLoader'),
  },
  {
    // Convert PO to JSON.
    loader: 'react-intl-po-loader',
  },
].filter(Boolean);

module.exports = {
  babelLoader,
  rootConfigBabelLoader,
  eslintLoader,
  makeCssLoader,
  makeSassLoader,
  sassVariablesLoader,
  makeAssetsLoader,
  svgReactLoader,
  poLoader,
};
