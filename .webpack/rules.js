const path = require('path');

const {
  babelLoader,
  rootConfigBabelLoader,
  eslintLoader,
  makeCssLoader,
  makeSassLoader,
  sassVariablesLoader,
  makeAssetsLoader,
  svgReactLoader,
  poLoader,
} = require('./loaders');

// Transpile using babel.
const babelRule = {
  test: /\.jsx?$/i,
  exclude: /(node_modules)/i,
  use: babelLoader,
};

const makeRootConfigBabelRule = ({
  nodeModules,
} = {}) => ({
  test: /\.jsx?$/i,
  use: rootConfigBabelLoader,
  include: nodeModules.map(nodeModule => path.resolve(
    __dirname,
    '..',
    'node_modules',
    nodeModule,
  )),
});

// Eslint.
const eslintRule = {
  test: /\.jsx?$/i,
  // Make sure that they are linted BEFORE they are transformed by babel.
  enforce: 'pre',
  // Do not lint files in node_modules.
  exclude: /(node_modules)/i,
  use: eslintLoader,
};

const makeCssRule = ({ mode } = {}) => ({
  test: /\.css$/i,
  exclude: /\.module\.css$/i,
  use: makeCssLoader({ mode }),
});

const makeCssModulesRule = ({ mode } = {}) => ({
  test: /\.module\.css$/i,
  use: makeCssLoader({ mode, enableModules: true }),
});

const makeSassRule = ({ mode } = {}) => ({
  test: /\.scss$/i,
  exclude: /\.(module|variables)\.scss$/i,
  use: makeSassLoader({ mode }),
});

const makeSassModulesRule = ({ mode } = {}) => ({
  test: /\.module\.scss$/i,
  use: makeSassLoader({ mode, enableModules: true }),
});

const sassVariablesRule = {
  test: /\.variables\.scss$/i,
  use: sassVariablesLoader,
};

// Rule for assets file and NODE_MODULES svg.
const makeAssetsRule = ({ mode } = {}) => ({
  test: /(node_modules[/\\].+\.svg)|(\.(jpg|jpeg|bmp|png|gif|eot|otf|ttf|woff|woff2|ico|pdf))$/i,
  use: makeAssetsLoader({ mode }),
});

// Rule for app svg files.
const svgReactRule = {
  test: /\.svg$/i,
  exclude: /node_modules/i,
  use: svgReactLoader,
};

const poRule = {
  test: /\.po$/i,
  use: poLoader,
};

module.exports = {
  babelRule,
  makeRootConfigBabelRule,
  eslintRule,
  makeCssRule,
  makeCssModulesRule,
  makeSassRule,
  makeSassModulesRule,
  sassVariablesRule,
  makeAssetsRule,
  svgReactRule,
  poRule,
};
