const path = require('path');

const { pickBy } = require('lodash');
const chalk = require('chalk');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlReplaceWebpackPlugin = require('html-replace-webpack-plugin');
const ModuleScopePlugin = require('react-dev-utils/ModuleScopePlugin');
const CaseSensitivePathsWebpackPlugin = require('case-sensitive-paths-webpack-plugin');
const WatchMissingNodeModulesPlugin = require('react-dev-utils/WatchMissingNodeModulesPlugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const errorOverlayMiddleware = require('react-dev-utils/errorOverlayMiddleware');
const noopServiceWorkerMiddleware = require('react-dev-utils/noopServiceWorkerMiddleware');
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');
const TerserWebpackPlugin = require('terser-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const webpack = require('webpack');

const {
  babelRule,
  makeRootConfigBabelRule,
  eslintRule,
  makeCssRule,
  makeCssModulesRule,
  makeSassRule,
  makeSassModulesRule,
  sassVariablesRule,
  makeAssetsRule,
  svgReactRule,
  poRule,
} = require('./.webpack/rules');

module.exports = (
  {
    bundleAnalysis = 'false',
    transpileNodeModules = 'auto',
    enableDevTools = 'auto',
  } = {},
  {
    mode = 'development',
  } = {},
) => {
  const shouldGenerateReport = bundleAnalysis === 'true';

  const shouldTranspileNodeModules = (
    transpileNodeModules === 'true'
    || (
      transpileNodeModules === 'auto'
      && mode === 'production'
    )
  );

  const shouldEnableDevTools = (
    enableDevTools === 'true'
    || (
      enableDevTools === 'auto'
      && mode !== 'production'
    )
  );

  /* eslint-disable no-console */
  console.log(chalk`Building for {cyan.bold ${mode}}`);
  console.log(`• Generate bundle analysis report: ${shouldGenerateReport ? chalk.green.bold('✔') : chalk.red.bold('Nope')}`);
  console.log(`• Transpile whitelisted node_modules: ${shouldTranspileNodeModules ? chalk.green.bold('✔') : chalk.red.bold('Nope')}`);
  console.log(`• Enable developer tools: ${shouldEnableDevTools ? chalk.green.bold('✔') : chalk.red.bold('Nope')}`);
  /* eslint-enable no-console */

  // Pick all variables starting with ENV_PREFIX from the environment.
  const appEnv = pickBy(
    process.env,
    (value, key) => key.toLowerCase().startsWith((process.env.ENV_PREFIX || '').toLowerCase()),
  );

  // Served from the root by webpack-dev-server in development and nginx in production.
  const publicPath = '/';

  // Point sourcemap entries to original disk location (format as URL on Windows).
  const devtoolModuleFilenameTemplate = info => path
    .relative('./src', info.absoluteResourcePath)
    .replace(/\\/g, '/');

  return {
    mode,

    devtool: 'source-map',

    // App entry point.
    entry: [
      // Load polyfills before the HMR client so it can be used on IE.
      './src/polyfills.js',

      // In development mode, add the HMR client.
      mode === 'development' && require.resolve('react-dev-utils/webpackHotDevClient'),

      // Application.
      './src/index.jsx',
    ].filter(Boolean),

    // Bundle output.
    output: mode === 'production'
      ? {
        path: path.join(__dirname, './.dist/'),
        filename: 'js/[name].[contenthash].min.js',
        chunkFilename: 'js/chunks/[name].[chunkhash:8].chunk.js',
        publicPath,
        devtoolModuleFilenameTemplate,
      }
      : {
        path: path.join(__dirname, './.dev/'),
        filename: 'js/bundle.min.js',
        chunkFilename: 'js/chunks/[name].chunk.js',
        publicPath,
        devtoolModuleFilenameTemplate,
      },

    module: {
      rules: [
        // JavaScript.
        eslintRule,
        babelRule,
        // Transpile specific node_modules with our babel config when necessary.
        shouldTranspileNodeModules && makeRootConfigBabelRule({
          nodeModules: [
            // Node modules that need to be transpiled for all environments.
            'ansi-styles',
            'ansi-regex',
            'strip-ansi',

            ...(
              mode === 'development'
                ? [
                  // Node modules that need to be transpiled in development mode only.
                  'chalk',
                  'react-dev-utils',
                ]
                : []
            ),
          ].filter(Boolean),
        }),

        // Sass, less and css.
        makeCssRule({ mode }),
        makeCssModulesRule({ mode }),
        makeSassRule({ mode }),
        makeSassModulesRule({ mode }),
        sassVariablesRule,

        // Assets.
        makeAssetsRule({ mode }),
        // App svg files (specific loader to have the actual <svg> tag in the DOM).
        svgReactRule,

        poRule,
      ].filter(Boolean),
    },

    resolve: {
      // When possible, use the "browser" field from the package.json to import modules.
      // This allows us to use packages that have both a browser and a Node.js version,
      // and to use the browser one.
      mainFields: [
        'browser',
        'main',
        'module',
      ],

      // Resolve absolute imports using these paths (in this order).
      modules: [
        './src/',
        './node_modules/',
      ],

      // Implicit extensions when omitted.
      extensions: [
        '.json',
        '.js',
        '.jsx',
      ],

      alias: {
        // React-intl messages are parsed at build time.
        'react-intl': 'react-intl/react-intl-no-parser.umd',
      },

      plugins: [
        // Prevent importing files outside of src/ (or node_modules/) except package.json.
        new ModuleScopePlugin('./src/', ['./package.json']),
      ],
    },

    plugins: [
      // Extract CSS to an external stylesheet. Better performance in production (allows caching).
      // In development we use style-loader to allow HMR with scss files.
      mode === 'production' && new MiniCssExtractPlugin({
        filename: 'css/[name].[contenthash].min.css',
        ignoreOrder: true,
      }),

      // Generate index.html linking to the generated bundles (js and css).
      new HtmlWebpackPlugin({
        filename: 'index.html',
        template: './src/index.html.ejs',
      }),

      // In production, variables are resolved at runtime by Nginx.
      // Replace variables for other environments at build time.
      // This means that you need to restart webpack when editing the environment.
      mode !== 'production' && new HtmlReplaceWebpackPlugin(Object.keys(appEnv).map(key => ({
        pattern: `$${key}`,
        replacement: appEnv[key],
      }))),

      // Define specific environment variables in the bundle.
      new webpack.DefinePlugin({
        // process.env.NODE_ENV is required by many packages.
        'process.env.NODE_ENV': JSON.stringify(mode),
        'process.env.ENV_PREFIX': JSON.stringify(process.env.ENV_PREFIX),
        'process.env.npm_package_name': JSON.stringify(process.env.npm_package_name),
        'process.env.npm_package_version': JSON.stringify(process.env.npm_package_version),
        __DEV_TOOLS_ENABLED__: shouldEnableDevTools ? 'true' : 'false',
      }),

      // HMR plugin.
      mode === 'development' && new webpack.HotModuleReplacementPlugin(),

      // Throw error when a required path does not match the case of the actual path.
      new CaseSensitivePathsWebpackPlugin(),

      // Trigger a new build when a node module package is installed.
      mode === 'development' && new WatchMissingNodeModulesPlugin(path.resolve('./node_modules/')),

      // Lint SCSS files.
      new StyleLintPlugin(),

      // If needed, generate a report to analyze why the bundle is large.
      shouldGenerateReport && new BundleAnalyzerPlugin({
        analyzerMode: 'static',
        reportFilename: path.resolve(path.join(__dirname, '.reports/bundle-analysis/bundle-analyzer-report.html')),
      }),
    ].filter(Boolean),

    // Some libraries import Node modules but don't use them in the browser.
    // Tell Webpack to provide empty mocks for them so importing them works.
    node: {
      dgram: 'empty',
      fs: 'empty',
      net: 'empty',
      tls: 'empty',
      child_process: 'empty',
    },

    optimization: {
      minimizer: [
        new TerserWebpackPlugin({
          sourceMap: true,
        }),

        new OptimizeCssAssetsWebpackPlugin({
          cssProcessorOptions: {
            map: {
              inline: false,
              annotation: true,
            },
          },
        }),
      ],

      splitChunks: {
        chunks: 'all',
        maxInitialRequests: 3,
        cacheGroups: {
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            priority: -10,
          },
          default: {
            minChunks: 2,
            priority: -20,
            reuseExistingChunk: true,
          },
        },
      },
    },

    // Dev server.
    devServer: mode === 'development'
      ? {
        publicPath,

        contentBase: './.dev',
        watchContentBase: true,
        watchOptions: {
          ignored: /node_modules/,
        },

        // Only show errors.
        stats: 'minimal',

        // Enable HMR.
        hot: true,
        // Since react-dev-utils now uses native WebSockets, we need to specify explicitly to use
        // WebSockets instead of sock-js.
        transportMode: 'ws',
        injectClient: false,

        // Proxy the requests using the variables used by nginx in production.
        proxy: {
          [process.env.NGINX_PROXY_PATH]: process.env.NGINX_PROXY_TARGET_URL,
        },

        // Serve index.html on 404.
        historyApiFallback: {
        // Paths with dots should still use the history fallback.
        // See https://github.com/facebookincubator/create-react-app/issues/387.
          disableDotRule: true,
        },

        // Use 8080 port as default.
        // Allow users to override via environment.
        port: +(process.env[`${process.env.ENV_PREFIX}WDS_PORT`] || 8080),

        /**
         * Add middlewares (probably). I copied that from create-react-app.
         *
         * @param {object} app - App.
         */
        before(app) {
        // This lets us open files from the runtime error overlay.
          app.use(errorOverlayMiddleware());
          // This service worker file is effectively a 'no-op' that will reset any
          // previous service worker registered for the same host:port combination.
          // We do this in development to avoid hitting the production cache if
          // it used the same host and port.
          // https://github.com/facebookincubator/create-react-app/issues/2272#issuecomment-302832432
          app.use(noopServiceWorkerMiddleware());
        },
      }
      : undefined,
  };
};
