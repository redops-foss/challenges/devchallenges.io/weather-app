module.exports = {
  verbose: true,

  // Exit on error.
  bail: true,

  // https://github.com/facebook/jest/issues/6769.
  testURL: 'http://localhost/',

  // Ignore node_modules test files.
  testPathIgnorePatterns: [
    '<rootDir>/node_modules/',
  ],

  coverageDirectory: '<rootDir>/.reports/coverage',

  collectCoverageFrom: [
    'src/**/*.{js,jsx}',
    '!src/**/__mocks__/**/*.{js,jsx}',
    '!src/**/__mockdata__/**/*.{js,jsx}',
    '!src/**/__mirage__/**/*.{js,jsx}',
    '!src/**/__tests_utils__/**/*.{js,jsx}',
    '!src/**/*enums.js',
    '!src/**/*constants.js',
    '!src/**/*.messages.js',
  ],

  // Absolute import paths.
  modulePaths: [
    '<rootDir>/src',
  ],

  moduleNameMapper: {
    // Do not fail on stylesheet or image files
    '\\.(css|scss|less|png)$': 'identity-obj-proxy',
    // use browser version of superagent otherwise miragejs will fail
    // see https://github.com/visionmedia/superagent/issues/1531
    superagent: 'superagent/dist/superagent',
  },

  // Transform with babel-jest.
  transform: {
    '^.+\\.(js|jsx)$': '<rootDir>/node_modules/babel-jest',
  },

  setupFiles: [
    '<rootDir>/.jest/setup.js',
  ],

  setupFilesAfterEnv: [
    '<rootDir>/.jest/setupAfterEnv.js',
  ],

  globals: {
    __DEV_TOOLS_ENABLED__: true,
  },
};
