# React boilerplate
> Boilerplate for a React project with redux, react-intl, storybook, eslint and many more tools.

# Deployments

- [Demo](https://redops-boilerplate-frontend-master.apps.innershift.sodigital.io/)
- [Storybook](https://redops:redops123@redops-boilerplate-storybook-master.apps.innershift.sodigital.io)
- [Coverage report](https://redops:redops123@redops-boilerplate-storybook-master.apps.innershift.sodigital.io/coverage/index.html)

## Requirements

1.  Install [Node >= 12](https://nodejs.org/en/download/)
(ideally install the latest LTS, currently Node 12).

1.  Install `yarn`
    ```bash
    npm install -g yarn
    ```

## Setup development environment

### Install project dependencies

```bash
# Go to project folder.
cd /project/path

# Install project dependencies.
yarn install
```

### Set environment variables

The environment variables in `.env` and `.env.local` files are set
before launching the dev server (see below to launch the dev server).

DO NOT set secrets in `.env` file. There should be only generic
environment variables (public variables that are common to every
developer of the project, such as `NODE_ENV`).

Your secrets must be set in `.env.local`. If you need to override
variables in `.env`, just override them in `.env.local`.
Consider removing them from `.env`, except if you want to set defaults.

```bash
# Copy the sample file (Obviously, don't do this if you already have a .env.local file).
cp .env.local.example .env.local
```

All variables are prefixed with `MY_APP_`, but you can change this prefix in `.env`.
If you change the prefix in the `.env` file, **DON'T FORGET TO CHANGE IT IN `index.html.ejs`**.

In the bundle, the only variables accessible are the ones you expose in `index.html.ejs` (see the exemple there).

Then replace the environment variables:

| Var name               | Prefixed | Description                                | Default value |
| ---------------------- | -------- | ------------------------------------------ | ------------- |
| ENV_PREFIX             |          | Prefix for app specific variables          | `'MY_APP_'`   |
| NGINX_PROXY_PATH       |          | The requests' urls you want to proxy       | `'/api'`      |
| NGINX_PROXY_TARGET_URL |          | The target of the proxy (your backend url) |               |
| WDS_PORT               | ✓        | Port used by `webpack-dev-server`          | `8080`        |

## Run development environment

```bash
# Launch webpack dev server with reverse proxy and hot module replacement.
yarn run dev
```

### Run development environment for IE/Edge

By default, the `yarn dev` script will not work on Internet Explorer and Edge,
because some modules are not transpiled in ES5.

To make it work, you can run the same script with the following option:
```bash
yarn run dev --env.transpileNodeModules=true
```

Unfortunately, this means that you need to manually whitelist any node module
that is not transpiled in ES5.
Whitelisted modules are set in `makeRootConfigBabelRule` in `webpack.config.js`.

## Build production package

```bash
yarn run build
```

Your bundle is now in `.dist/`.

### Note for IE/Edge

Some production modules are not transpiled in ES5. By default when building
for production, whitelisted modules are transpiled, but you need to specify
which modules must be transpiled manually in the configuration, in
`makeRootConfigBabelRule` in `webpack.config.js`.

## Build docker image

```bash
# Build production package.
yarn run build

# Build docker image.
docker build \
  --build-arg APP_EXPOSED_PORT=8080 \
  --build-arg APP_DIST_FOLDER=./.dist \
  --build-arg APP_NGINX_FOLDER=./.nginx \
  -t <YOUR_IMAGE_NAME> \
  .

# Or use docker-compose
docker-compose up -d app
```

## Run production

### Setup environment

Setup environment variables via `.env` files before launching the container.

### Run

Assuming the `.env` and `.env.local` files contain your **production** configuration:
```bash
docker run \
  --name <YOUR_CONTAINER_NAME> \
  --env-file .env \
  --env-file .env.local \
  -d \
  <YOUR_IMAGE_NAME>
```
