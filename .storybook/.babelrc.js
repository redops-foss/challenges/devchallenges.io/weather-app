const fs = require('fs');
const path = require('path');

// For some reason I can't require the babelrc directly, so just read and parse it.
const babelRc = JSON.parse(fs.readFileSync(
  path.join(__dirname, '../.babelrc'),
  'utf8',
));

// If either of these plugins is set to loose mode by storybook,
// all the others must be in loose mode as well.
const mustBeLoose = [
  '@babel/plugin-proposal-class-properties',
  '@babel/plugin-proposal-private-methods',
  '@babel/plugin-proposal-private-property-in-object',
];

// Use our development config, and set the required plugins in loose mode.
module.exports = {
  ...babelRc.env.development,
  plugins: (babelRc.env.development.plugins || []).map((plugin) => {
    // A plugin can be written as "plugin" or ["plugin", config].
    const [pluginName, pluginConfig = {}] = Array.isArray(plugin)
      ? plugin
      : [plugin];

    return mustBeLoose.includes(pluginName)
      ? [pluginName, { ...pluginConfig, loose: true }]
      : plugin;
  }),
};
