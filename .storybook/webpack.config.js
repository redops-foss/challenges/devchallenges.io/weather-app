const webpack = require('webpack');

const {
  makeCssRule,
  makeCssModulesRule,
  makeSassRule,
  makeSassModulesRule,
  sassVariablesRule,
  makeAssetsRule,
  svgReactRule,
  poRule,
} = require('../.webpack/rules');

module.exports = ({ config }) => {
  // console.error(JSON.stringify(config, null, 2));

  // console.error(config.module.rules);

  // This makes the alias only work when importing from 'react' exactly.
  // This will work as long as storybook does not import from react like 'react/lib/x'.
  config.resolve.alias.react$ = config.resolve.alias.react;
  delete config.resolve.alias.react;

  config.devServer = { stats: 'minimal' };

  // Disable dev tools in storybook.
  config.plugins.push(new webpack.DefinePlugin({
    __DEV_TOOLS_ENABLED__: 'false',
  }));

  config.module.rules = [
    // JS/JSX.
    config.module.rules[0],
    config.module.rules[1],
    config.module.rules[3],

    // MDX.
    config.module.rules[4],
    config.module.rules[5],

    // JS/JSX stories.
    config.module.rules[6],

    // Raw loader.
    config.module.rules[2],

    // Files (mp3 etc.).
    config.module.rules[9],

    makeCssRule(),
    makeCssModulesRule(),
    makeSassRule(),
    makeSassModulesRule(),
    sassVariablesRule,

    makeAssetsRule(),
    svgReactRule,

    poRule,
  ];

  // console.log(JSON.stringify(config, null, 2));

  return config;
};
