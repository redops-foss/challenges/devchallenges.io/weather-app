const path = require('path');

module.exports.EXTRACTED_MESSAGES_FILE = path.join(__dirname, '..', '..', '.tmp/intl/messages.json');
module.exports.TEMPLATE_FILE = path.join(__dirname, '..', '..', '.tmp/intl/messages.pot');
module.exports.PO_FILES_PATTERN = 'src/intl/locales/**/messages.po';
