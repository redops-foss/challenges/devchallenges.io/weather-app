#!/usr/bin/env node
const { argv } = require('yargs');

const generatePotFile = require('./generatePotFile');
const createPoFile = require('./createPoFile');
const updatePoFiles = require('./updatePoFiles');
const cleanPoFiles = require('./cleanPoFiles');

const cli = async () => {
  switch (argv.a) {
    case 'intl:generatePotFile':
      return generatePotFile(argv.i, argv.t);
    case 'intl:createPoFile':
      return createPoFile(argv.d, argv.l, { templateFile: argv.t });
    case 'intl:updatePoFiles':
      return updatePoFiles(argv.p, { templateFile: argv.t });
    case 'intl:cleanPoFiles':
      return cleanPoFiles(argv.p);
    default:
      throw Error(`scripts/intl: unknown command "${argv.a}"`);
  }
};

cli()
  .catch((error) => {
    // eslint-disable-next-line no-console
    console.error(error);

    // Exit on error to prevent commiting an invalid state.
    process.exit(1);
  });
