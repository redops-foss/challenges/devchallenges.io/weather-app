const path = require('path');

const fse = require('fs-extra');
const shell = require('shelljs');

const { TEMPLATE_FILE } = require('./constants');

const createPoFile = async (
  directory,
  locale,
  { templateFile = TEMPLATE_FILE } = {},
) => {
  if (!shell.which('msginit')) {
    shell.echo('Sorry, this script requires msginit. Skipping...');
    shell.exit(0);
  }

  if (!locale) {
    throw new Error('Missing parameter locale. Use `yarn intl:create -l <locale>`');
  }

  const poDirectory = path.join(directory, locale);
  await fse.ensureDir(poDirectory);

  const poFile = path.join(poDirectory, 'messages.po');

  return new Promise((resolve, reject) => {
    if (!locale) {
      reject(new Error('Missing parameter locale. Use `yarn intl:create -l <locale>`'));
      return;
    }

    shell.exec(
      `msginit --no-translator -i ${templateFile} -o ${poFile} --locale=${locale}`,
      { silent: true },
      (code, stdout, stderr) => {
        if (code !== 0) {
          return reject(stderr);
        }
        return resolve(stdout);
      },
    );
  });
};

module.exports = createPoFile;
