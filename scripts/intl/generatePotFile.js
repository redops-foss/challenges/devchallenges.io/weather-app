const fse = require('fs-extra');

const { EXTRACTED_MESSAGES_FILE, TEMPLATE_FILE } = require('./constants');

/**
 * Generate POT header.
 *
 * @returns {string} POT header.
 */
const generatePotHeader = () => [
  'msgid ""',
  'msgstr ""',
  `"POT-Creation-Date: ${new Date().toISOString()}\\n"`,
  '"Content-Type: text/plain; charset=UTF-8\\n"',
  '"Content-Transfer-Encoding: 8bit\\n"',
  '"MIME-Version: 1.0\\n"',
  '"X-Generator: redops-pot-generator\\n"',
].join('\n');

/**
 * Format a react-intl message to PO message format.
 *
 * @param {string} id - Message id.
 * @param {object} msg - React-intl extracted message.
 * @returns {string} PO message.
 */
const formatPoMsg = (id, msg) => [
  msg.description && `#. ${msg.description}`,
  msg.file && `#: ${msg.file}:${msg.line}`,
  `msgctxt "${id.replace(/"/g, '\\"')}"`,
  `msgid "${msg.defaultMessage.replace(/"/g, '\\"')}"`,
  'msgstr ""',
].filter(Boolean).join('\n');

/**
 * Generate a POT file from a list of messages.
 *
 * @param {string} extractedMessagesFile - Path to the input extracted messages file.
 * @param {string} templateFile - Path to the output POT file.
 */
const generatePotFile = async (
  extractedMessagesFile = EXTRACTED_MESSAGES_FILE,
  templateFile = TEMPLATE_FILE,
) => {
  // Read the input file to get the list of messages.
  const extractedMessages = await fse.readJSON(extractedMessagesFile, { encoding: 'utf8' });

  // Generate the output.
  const output = [
    generatePotHeader(),

    // Sort messages alphabetically and convert to PO format.
    ...Object.keys(extractedMessages)
      .sort((idA, idB) => {
        const msgA = extractedMessages[idA].defaultMessage;
        const msgB = extractedMessages[idB].defaultMessage;
        return msgA.localeCompare(msgB, 'en', { numeric: true });
      })
      .map(id => formatPoMsg(id, extractedMessages[id])),
  ].join('\n\n');

  // Write to output POT file.
  await fse.writeFile(
    templateFile,
    `${output}\n`,
    { encoding: 'utf8' },
  );
};

module.exports = generatePotFile;
