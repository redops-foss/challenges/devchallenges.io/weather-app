// Polyfills.
import 'core-js/stable';
import 'regenerator-runtime/runtime';

// Bootstrap.
import dayjs from 'dayjs';

import bootstrapDayJs from '../src/services/dayjs/bootstrap';

bootstrapDayJs(dayjs);
